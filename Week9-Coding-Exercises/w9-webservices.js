let map;
let myArray = [];

//Task 1
if('geolocation' in navigator) {
    /* geolocation is available */
    navigator.geolocation.getCurrentPosition(success, error)
  } else {
    /* geolocation IS NOT available */
    error()
  }

function success(location){
    //Task 2

    mapboxgl.accessToken = 'pk.eyJ1Ijoia3RvbjAwMDkiLCJhIjoiY2tzang5bzA1MTU5MjJ2bnNveG5keW1kNSJ9.mAOYcXRR2hUDmpr_mS2Tdw';
    map = new mapboxgl.Map({
        container: 'map', // container ID
        style: 'mapbox:// styles/mapbox/streets-v11', // style URL
        center: [location.coords.longitude, location.coords.latitude], // starting position [lng, lat]
        zoom: 12 // starting zoom
    });
    
    bounds()
}
function bounds(){
    for(let i = 0; i < myArray.length; i++){
        myArray[i].remove()
    }
    
    myArray = []
    //Task 3
    let Boundaries = map.getBounds()
    //Task 4
    let latlng = Boundaries._ne.lat + ", " + Boundaries._ne.lng + ", " + Boundaries._sw.lat + ", " + Boundaries._sw.lng
    
    let queryString = "https://api.waqi.info/map/bounds/?token=003735d4adb0cf5cfa7d99fe7857adf77c81a020&latlng="+ latlng +"&callback=display"
    
    let script = document.createElement("script");
    script.src = queryString;
    document.body.appendChild(script)
}

function display(result){
    console.log(result.data[0])
    
    for(let i = 0; i < result.data.length; i++){
        
        myArray.push(new mapboxgl.Marker({
            color: AQItoColour(result.data[i].aqi),
            }).setLngLat([result.data[i].lon, result.data[i].lat]) //longlat
            .addTo(map));
        
        myArray.push(new mapboxgl.Popup({offset: 30})
        .setLngLat([result.data[i].lon, result.data[i].lat])
        .setHTML("<h5>" + station.name + "</h5>")
        .addTo(map));
    }
    
    map.on('zoomend',function(){
        bounds()
    });
}

function AQItoColour(aqi){
    let colour;
    
    if(aqi > 0 && aqi < 50){
        colour = "green"
    } else if(aqi > 51 && aqi < 100){
        colour = "yellow"
    } else if(aqi > 101 && aqi < 150){
        colour = "orange"
    } else if(aqi > 151 && aqi < 200){
        colour = "red"
    } else if(aqi > 201 && aqi < 300){
        colour = "purple"
    } else if(aqi > 301 && aqi < 500){
        colour = "maroon"
    } else{
        colour = "white"
    }
    return colour
}

function error(){
    alert("Geolocation IS NOT available")
}