"use strict";


function addStudent(){
    let fullName = document.getElementById("fullName");
    let fullName_msg = document.getElementById("fullName_msg");
    let studentId = document.getElementById("studentId");
    let studentId_msg = document.getElementById("studentId_msg");
    let problem = document.getElementById("problem");
    let problem_msg = document.getElementById("problem_msg");
    const regex = /^[1-3]{1}[0-9]{7}$/;
    
    if(fullName.value==""){
        fullName_msg.innerHTML="Full Name cannot be empty";
        return false;
    } else {
        fullName_msg.innerHTML="";
        }
    
    if(studentId.value.match(regex)){
        studentId_msg.innerHTML="";
    } else {
        studentId_msg.innerHTML="Student id is not an integer";
        return false;
        }
    if(problem.value==""){
        problem_msg.innerHTML="Problem cannot be empty";
        return false;
    } else {
        problem_msg.innerHTML="";
        }
    
    localStorage.setItem("fullName",fullName.value);
    localStorage.setItem("studentId",studentId.value);
    localStorage.setItem("problem",problem.value);
    localStorage.setItem("addStudent",JSON.stringify(Student))
    alert("Student sucessfully added");
    window.location="KounMingTong_index.html";
    return true;
}


    