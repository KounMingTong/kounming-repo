"use strict";
// Keys for localStorage
const STUDENT_INDEX_KEY = "studentIndex";
const STUDENT_QUEUE_KEY = "queueIndex";
const APP_DATA_KEY = "consultationAppData";

class student {
    constructor(fullName, studentId, problem){
        this._fullName = fullName;
        this._studentId = studentId;
        this._problem = problem;
    }
    
    get fullName() {
        return this._fullName;
    }
    
    get studentId() {
        return this._studentId;
    }
    
    get problem() {
        return this._problem;
    }
    
    fromData() {
        this._fullName = student._fullName;
        this._studentid = student._studentid;
        this._problem = student._problem;
    }
}

class session {
    constructor() {
        this._startTime = new Date();
        this._queue = new Array();
    }
    
    get startTime() {
        return this._startTime.toLocaleDateString();
    }
    
    get queue() {
        return this._queue;
    }
    
    addSubQueue() {
        this._queue.push(new Array());
    }
    
    addStudent(fullName,studentId,problem) {
        let subQueue;
        if(this._queue.length < subQueue.length){
            this._queue.push(student)
        } else {
            this.subQueue.push(student)
        }
        
    
    removeStudent(studentIndex,queueIndex) {
        this._queue[queueIndex].remove(studentIndex);
    }
    
    getStudent(studentIndex, queueIndex) {
        return this._queue[queueIndex].get(studentIndex);
    }
    
    fromData(session) {
        this._startTime = new Date(session.startTime);
        
        this._queue = new Array();
        for (let queue in session.queue){
            let duplicateQueue = new Array();
            for (let student of queue) {
                duplicateQueue.push(student.fromData());
            }
            this._queue.push(duplicateQueue);
        }
    }
}

function checkLocalStorageDataExist(key) {
    if (localStorage.getItem(key)) {
        return true;
    }
    return false;
}

function updateLocalStorageData(key, data) {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
}

function getLocalStorageData(key) {
    let jsonData = localStorage.getItem(key);
    let data = jsonData;

    try {
        data = JSON.parse(jsonData);
    }
    
    catch(e) {
        console.error(e);
    }
    
    finally {
        return data;
    }
}

let consultSession = new session();
if (checkLocalStorageDataExist(APP_DATA_KEY)) {
    // if LS data does exist
    let data = getLocalStorageData(APP_DATA_KEY);
    consultSession.fromData(data);
    } else {
        // if LS data doesn’t exist
        consultSession.addSubQueue();
        consultSession.addSubQueue();
        updateLocalStorageData(APP_DATA_KEY, consultSession);
    }
