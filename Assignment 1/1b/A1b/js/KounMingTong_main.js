"use strict";

function updateDayTime(){
    let currentTime = "";
    currentTime = new Date().toLocaleTimeString();
    document.getElementById("currentTime").innerHTML = currentTime;
}
setInterval(updateDayTime,1000);

function view(index,queueIndex){
    localStorage.setItem("STUDENT_INDEX_KEY",index);
    localStorage.setItem("STUDENT_QUEUE_KEY",queueIndex);
    window.location.href = 'KounMingTong_view.html'
}

function markDone(index,queueIndex){
    if (confirm("Mark this student as ‘done’.") == true){
        consultSession.removeStudent(index,queueIndex);
        updateLocalStorageData(APP_DATA_KEY,consultSession);   
    }
}

