/* 
    Author: KounMing Tong
    ID: 32222009
    
    1. Building a simple single page web app that will run and allow users to book a specific room between the hours of 8AM and 6PM, in 1 hour blocks. 
    2. Creating one HTML and one JS file. Please name them as follows:
            · [YourName]_index.html
            · [YourName]_main.js
    3. bookingData variable is given and it stores the booking data for a given room in the system. 
        Each item in the array contains an object with the information of the booking status of that time slot. The timeslot of the object corresponds to the index in the array.    
    4. This information consists of:
            · time: the starting time in 24-hour time of the time slot
            · reason: the reason for this booking, which is stored internally and not shown to users
            · label: the publicly shown description for the booking
            · booked: a boolean flag to indicate if the room is currently booked or not
*/

"use strict";
let bookingData = [,,,,,,,,
        {
            time: "08:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "09:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "10:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "11:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "12:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "13:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "14:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "15:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "16:00",
            reason: "",
            label: "",
            booked: false
        },{
            time: "17:00",
            reason: "",
            label: "",
            booked: false
        }
    ];

updateDisplay();        // Call function to print booking list on first load


/* 
    This function is to assign the users time, reason and label into bookingData.
    1. Create bookRoom function with parameters (time,reason,label)
    2. Loop for the bookingData, check if the bookingData time is match with user's selected time.
    3. Reassign the object, assign the booked into true.
*/
function bookRoom(time,reason,label){
    for (let i = 8; i < bookingData.length; i++){       // i started from 8 as the array has empty string from 0-7
        if(bookingData[i].time === time){
            bookingData[i].reason = reason;
            bookingData[i].label = label;
            bookingData[i].booked = true;
        }
    }
}


/* 
    This function is to check if the slot is available from bookingData
    1. Create checkRoomBooked function with parameters (time)
    2. Loop for the bookingData, check if the bookingData time is match with user's selected time.
    3. Return the value (logically true means Not Available, false mean available)
*/
function checkRoomBooked(time){
    for (let i = 8; i < bookingData.length; i++){       // i started from 8 as the array has empty string from 0-7
        if (bookingData[i].time === time)
            {
                return bookingData[i].booked;
            }
    }
}


/* 
    This function is to clear all bookings
    1. Create clearRoomBookings function
    2. Reassign all reason and label into empty string, and booked into false
*/
function clearRoomBookings(){
    for (let i = 8; i < bookingData.length; i++){       // i started from 8 as the array has empty string from 0-7
        bookingData[i].reason = "";
        bookingData[i].label = "";
        bookingData[i].booked = false;
    }
    updateDisplay();        // Call the function to update the latest booking data on page
}

/* 
    This function is to print latest booking status on page
    1. Create clearRoomBookings function
    2. Assign variable status and updateList
    3. Loop for the bookingData, check if the all elements in bookingData is available or booked
    4. Print the booking time with status on page
*/
function updateDisplay(){
    let status;
    let updateList = "";        // Only working with empty string without error
    for (var i = 8; i < bookingData.length; i++){       // i started from 8 as the array has empty string from 0-7
        if(bookingData[i].booked === false){        // Show its status and label by checking the .booked
            status = " Available";
        } else {
            status = " Not Available (" + bookingData[i].label + ")";
           }
        updateList += "<p>" + bookingData[i].time + status + "<br /></p>";      // Assign all time slot with status and label to updateList
    }
    document.getElementById("output").innerHTML = updateList;       // Print the updateList to page
}


/* 
    This function is to interacting with DOM in order to input user's data
    1. Create doBooking function
    2. Assign user's input to variable time, label and reason
    3. Check if the all details from page is filled in, then check the time slot if it is available
    4. Call bookRoom function to update the bookingData
*/
function doBooking(){
    let time = document.getElementById("inputTime").value;      // Assign User input (time) with attribute value
    let label = document.getElementById("inputReason").value;   // Assign User input (reason) with attribute value
    let reason = document.getElementById("inputLabel").value;   // Assign User input (label) with attribute value
    if (time === "" || label === "" || reason === ""){
            alert("Please fill all details!");      // Shows alert when there is any blank without filled in and return
            return;
    } else {
        if(checkRoomBooked(time)){
            alert("Please choose other time slot, it is already booked!");      // Shows alert when it is booked
        } else {
            bookRoom(time,reason,label);
            updateDisplay();        // Call the function to update the latest booking data on page
        }
    }
}


/* 
    This function is to interacting with DOM in order to clear all bookings
    1. Create clearAllBookings function
    2. Check if user wants to comfirn to clear
    3. Call clearRoomBooked to clear
*/
function clearAllBookings(){
    if(confirm("Are you sure to clear all bookings?")){     // Ask confirmation from window confirm method
        clearRoomBookings();
        updateDisplay();        // Call the function to update the latest booking data on page
       }
}


/* 
    This function is to print the current time
    1. Create updateDayTime function
    2. Assign time to variable timeNow
    3. Print timeNow into Page
*/
function updateDayTime(){
    let timeNow = "";
    timeNow = new Date().toLocaleTimeString();
    document.getElementById("timeNow").innerHTML = timeNow;
}
setInterval(updateDayTime,1000);        // Update time every 1000ms