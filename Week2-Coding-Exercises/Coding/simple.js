//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "";  //empty output, fill this so that it can print onto the page.
    
    //Question 1 here
    let positiveOdd = [];
    let negativeEven = [];
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    
    for (let i = 0; i<myArray.length; i++){
        if (myArray[i] > 0 && myArray[i]%2 !== 0){
            positiveOdd += myArray[i] + ' ';
        } if (myArray[i] < 0 && myArray[i]%2 === 0){
            negativeEven += myArray[i] + ' ';
        }
    }
    
    // Output
    output = 'Positive Odd: ' + positiveOdd + '\n' + 'Negative Even: ' + negativeEven
    
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = ""
    let tempOutput = [0,0,0,0,0,0] // temp place
    let randomNum;
    
    //Question 2 here 
    for (let i = 0; i < 60000; i++ ){
        randomNum = Math.floor((Math.random() * 6) + 1) // value generate randomly from 1 to 6
        if (randomNum === 1){
            tempOutput[0]++ ;
        }
        else if (randomNum === 2){
            tempOutput[1]++;
        }
        else if (randomNum === 3){
            tempOutput[2]++;
        }
        else if (randomNum === 4){
            tempOutput[3]++;   
        }
        else if (randomNum === 5){
            tempOutput[4]++;   
        }
        else if (randomNum === 6){
            tempOutput[5]++;   
        }
    }
    
    // Output
    output += "Frequency of die rolls" + "\n";
    output += "1: " + tempOutput[0] + "\n";
    output += "2: " + tempOutput[1] + "\n";
    output += "3: " + tempOutput[2] + "\n";
    output += "4: " + tempOutput[3] + "\n";
    output += "5: " + tempOutput[4] + "\n";
    output += "6: " + tempOutput[5] + "\n";
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = ""
    let tempOutput =['','','','','','','','']  // temp place
    let randomNum;
    
    //Question 3 here
        for (let i = 0; i < 60000; i++ ){
        randomNum = Math.floor((Math.random() * 6) + 1) // value generate randomly from 1 to 6
        if (randomNum === 0){
            tempOutput[0] ++;
        }
        else if (randomNum === 1){
            tempOutput[1]++;
        }
        else if (randomNum === 2){
            tempOutput[2]++;
        }
        else if (randomNum === 3){
            tempOutput[3]++;
        }
        else if (randomNum === 4){
            tempOutput[4]++;   
        }
        else if (randomNum === 5){
            tempOutput[5]++;   
        }
        else if (randomNum === 6){
            tempOutput[6]++;
        }
    }
    // Output
    output += "Frequency of die rolls" + "\n";
    
    for (var index = 1; index <= 6; index++){
        output += index + ": " + tempOutput[index] + "\n"
    }
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    let randomNum;
    //Question 4 here 
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions: ""
}
    for (let i = 0; i <= dieRolls.Total; i++){
        randomNum = Math.floor((Math.random()*6)+1) // value generate randomly from 1 to 6
        dieRolls.Frequencies[randomNum]++;
    }
    
    let count = 10000
    let exceptionOccurs = count * 0.01;
    
    for (let index = 1; index <= 6; index++){
        let difference = dieRolls.Frequencies[index] - count;
        if (Math.abs(difference) > exceptionOccurs){
            dieRolls.Exceptions += index + " ";
        }
    }
    
    // Output
    output += "Frequency of dice rolls" + "\n"
    output += "Total rolls: " + dieRolls.Total + "\n";
    output += "Frequencies: " + "\n";
    for (let index in dieRolls.Frequencies){
        output += index + ": " + dieRolls.Frequencies[index] + "\n"
        }
    output += "Exceptions: " + dieRolls.Exceptions
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}


function question5(){
    let output = "" 
    
    //Question 5 here 
    let person = {
            name: "Jane",
            income: 127050
    }
    let tax;
    
    if (person.income <= 18200){
        tax = 0;
    } else if (person.income > 18200 && person.income <= 37000){
        tax = 0.19 * (person.income - 18200)
    } else if (person.income > 37000 && person.income <= 90000){
        tax = 3572 + (0.325 * (person.income - 37000))
    } else if (person.income > 90000 && person.income <= 180000){
        tax = 20797 + (0.37 * (person.income - 90000))
    } else {
        tax = 54097 + (0.45 * (person.income - 180000))
    }
    
    let tax2nd = tax.toFixed(2); // 2 decimal places
    // Output
    output += person.name + "'s income is: $" + person.income + ", and their tax owed is: $ " + tax2nd
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}