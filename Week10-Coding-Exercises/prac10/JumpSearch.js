/* Write your function for implementing the Jump Search algorithm here */

    function jumpSearchTest(data) {
    // Initialize block size and starting point
    // block size = blockSize = 4; Starting point = currentIndex = 0
	let blockSize = 4;
    let currentIndex = 0;

    // If the starting point is less than the array length, checking the data input by incrementing point with block size, if the first jump search is complete, then increment the starting point by one and repeat the process, if the data is not in the array, return null
    for (let i = 0; i < blockSize; i++) {
        let checkDone = false;
        for (let j = 0; j < data.length; j += blockSize) {
            if (data[currentIndex + j].emergency) {
                checkDone = true;
                return data[currentIndex + j].address;
            }
        }
        if (!checkDone) {
            currentIndex++;
        } else {
            return null;
        }
    }
}
