/* Write your function for implementing the Serial Search Algorithm here */
function serialSearchTest(data)
{
    //Initializing the starting index at 0
    for (let index = 0; index < ARRAY_SIZE;)
    {
        if (data[index].emergency === true)
        {
            return data[index].address
        }
        else if(data[index].emergency === false)
        {
            index += 1;
        }
        else
        {
            return null
        }
    }
}