// Setup
const ARRAY_SIZE = 300000;
let outputAreaRef = document.getElementById("outputArea");
outputAreaRef.innerHTML += ARRAY_SIZE+ " houses setting up and ready to go.<br/>";
// Generating array
let array =[];
for (let i = 0; i < ARRAY_SIZE; i++)
{
  let house ={
    address:i,
    emergency:false
  };
  array.push(house);
}
// Randomly set one house to true for emergency
let centerIndex = ARRAY_SIZE - 1;
let randomAddress = Math.floor(Math.random()*centerIndex);
array[randomAddress].emergency=true;
console.log(randomAddress);
// Randomise (shuffle) array contents
while (centerIndex > 0)
{
  let i = Math.floor(Math.random() * centerIndex);
  let tempItem = array[centerIndex];
  array[centerIndex] = array[i];
  array[i] = tempItem;
  centerIndex--;
}
// startTest runs when button clicked
function startTest()
{ 
    /**
  let findIndex = findIndexTest(array);
  outputAreaRef.innerHTML += "Find Index - done. Searched address is at "+findIndex+"<br/>";
     **/
    
    // Bidirectional is the fastest as it takes the average time of 0.26ms while serial and jumpSearch have 2.22ms and 1.8ms seperately. Bidirectional is O(n^(s/2)) which s is the swallowest goal, serial search has the time and complexity of O(n) while jump search has the time and complexity of O(sqrt(n)), bidirectional in this case will dramatically reduce the storage taken in searching and hence the time.
    
    
  let serialSearch = serialSearchTest(array);
  outputAreaRef.innerHTML += "Serial Search - done. Searched address is at "+serialSearch+"<br/>";
    
  let bidirectionalSearch = bidirectionalSearchTest(array);
  outputAreaRef.innerHTML += "Bidirectional Search - done. Searched address is at "+bidirectionalSearch+"<br/>";
    
  let jumpSearch = jumpSearchTest(array);
  outputAreaRef.innerHTML += "Jump Search - done. Searched address is at "+jumpSearch+"<br/>";
    
  outputAreaRef.innerHTML += "Testing completed. You may now stop the Google Chrome Profiler and review the results.";
}