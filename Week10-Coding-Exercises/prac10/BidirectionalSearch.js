/* Write your function for implementing the Bi-directional Search algorithm here */

// Algorithm C - Biderectional Search

function bidirectionalSearchTest(array) {
    
    let middleOfArray = array.length / 2
    
    for (let counter = 0; counter < middleOfArray; counter++) {
        
        if (array[middleOfArray - counter].emergency === true) {
            return array[middleOfArray - counter].address;
        }
        
        if (array[middleOfArray + counter].emergency === true) {
            return array[middleOfArray + counter].address;
        } 
    }
    return null;
}