// Programming Task 1

// Creating Variables
r = 4;  // Radius
pi = Math.PI;   // Pi
c = 2*pi*r; //  Circumference
let c_2d = c.toFixed(2); // Circumference in 2 decimal places

// Print
console.log(c_2d)